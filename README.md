# LED Matrix display NERVA Monitor

## LED Matrix display NERVA Monitor

This can monitor netowrk hash rate and mining hash rate from a LED matrix display using arduino. 
Backend python program is used to send data to the serial port of the arduino board.

![](1.gif)

## Flip

To display Hash rate

![](2.gif)

Library for LED MATRIX display

https://github.com/MajicDesigns/MD_MAX72XX

Circuit Diagram

![](CIR1.JPG)

## Hardware List

Dot led matrix MCU control LED Display module MAX7219 for Arduino

https://www.ebay.com/itm/Dot-led-matrix-MCU-control-LED-Display-module-MAX7219-for-Arduino-Raspberry-Pi-M/302522228880?hash=item466fbae090:g:e9wAAOSwUwFaBAno:rk:1:pf:1&frcectupt=true

UNO R3 ATMEGA328P-16AU CH340G Micro USB With Cable Board

https://www.ebay.com/itm/UNO-R3-ATMEGA328P-16AU-CH340G-Micro-USB-With-Cable-Board-For-Arduino-NEW/263884202082?hash=item3d70b96062:g:XTEAAOSwGCNbdkzE:rk:2:pf:1&frcectupt=true

