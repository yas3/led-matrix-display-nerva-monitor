import serial
import time
import os
import re


ser=serial.Serial('/dev/ttyACM0')
print("Connecting to")
print(ser.name)
time.sleep(2) 
ser.write(b"Nerva")
ser.write('\n')
while(1):
	msg= os.popen("./nervad status").read()
	result_HR=re.search('mining at ([0-9.]+ k?H/s)',msg)
	result_NH=re.search('net hash ([0-9.]+ k?M?H/s)',msg)
	
	if result_NH and result_HR:
		print(result_NH.group())
		print(result_HR.group())
		ser.write("NERVA ")
		ser.write("Net: ")
		ser.write(str(result_NH.group(1)))
		ser.write(" H: ")
		ser.write(str(result_HR.group(1)))
		ser.write('\n')
	else:
		ser.write("Err")
		ser.write('\n')
	
	time.sleep(20) 

	
