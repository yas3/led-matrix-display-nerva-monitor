#include <MD_MAX72xx.h>
#include <SPI.h>

#define USE_POT_CONTROL 0
#define PRINT_CALLBACK 0

#define PRINT(s, v)     \
  {                     \
    Serial.print(F(s)); \
    Serial.print(v);    \
  }

// Define the number of devices we have in the chain and the hardware interface
// NOTE: These pin numbers will probably not work with your hardware and may
// need to be adapted
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 8

#define CLK_PIN 13  // or SCK
#define DATA_PIN 11 // or MOSI
#define CS_PIN 10   // or SS

// SPI hardware interface
MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);
// Arbitrary pins
//MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);

// Scrolling parameters
#if USE_POT_CONTROL
#define SPEED_IN A5
#else
#define SCROLL_DELAY 100 // in milliseconds
#endif                   // USE_POT_CONTROL

#define CHAR_SPACING 1 // pixels between characters

// Global message buffers shared by Serial and Scrolling functions
#define BUF_SIZE 75
char newMessage[BUF_SIZE];
bool newMessageAvailable = false;
int counter = 0;
char buff[6][2];
int digit[6];
int digit_old[6];
unsigned long timer1 = 0;
unsigned long delay1 = 30;
unsigned long timer2 = 0;



void printText(uint8_t modStart, uint8_t modEnd, char *pMsg)
// Print the text string to the LED matrix modules specified.
// Message area is padded with blank columns after printing.
{
  uint8_t state = 0;
  uint8_t curLen;
  uint16_t showLen;
  uint8_t cBuf[8];
  int16_t col = ((modEnd + 1) * COL_SIZE) - 1;

  mx.control(modStart, modEnd, MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);

  do // finite state machine to print the characters in the space available
  {
    switch (state)
    {
      case 0: // Load the next character from the font table
        // if we reached end of message, reset the message pointer
        if (*pMsg == '\0')
        {
          showLen = col - (modEnd * COL_SIZE); // padding characters
          state = 2;
          break;
        }

        // retrieve the next character form the font file
        showLen = mx.getChar(*pMsg++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);
        curLen = 0;
        state++;
      // !! deliberately fall through to next state to start displaying

      case 1: // display the next part of the character
        mx.setColumn(col--, cBuf[curLen++]);

        // done with font character, now display the space between chars
        if (curLen == showLen)
        {
          showLen = CHAR_SPACING;
          state = 2;
        }
        break;

      case 2: // initialize state for displaying empty columns
        curLen = 0;
        state++;
      // fall through

      case 3: // display inter-character spacing or end of message padding (blank columns)
        mx.setColumn(col--, 0);
        curLen++;
        if (curLen == showLen)
          state = 0;
        break;

      default:
        col = -1; // this definitely ends the do loop
    }
  } while (col >= (modStart * COL_SIZE));

  mx.control(modStart, modEnd, MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
}

void readSerial(void)
{
  static uint8_t  putIndex = 0;

  while (Serial.available())
  {
    newMessage[putIndex] = (char)Serial.read();
    if ((newMessage[putIndex] == '\n') || (putIndex >= BUF_SIZE - 3)) // end of message character or full buffer
    {
      // put in a message separator and end the string
      newMessage[putIndex++] = ' ';
      newMessage[putIndex] = '\0';
      // restart the index for next filling spree and flag we have a message waiting
      putIndex = 0;
      newMessageAvailable = true;
    }
    else if (newMessage[putIndex] != '\r')
      // Just save the next char in next location
      putIndex++;
  }
}

void setup()
{
  Serial.begin(9600);
  mx.begin();
  mx.control(MD_MAX72XX::INTENSITY, 1);

  printText(0, 0, "0");
  printText(1, 1, "0");
  printText(2, 2, "0");
  printText(3, 3, "0");


}



void displayValue(int counter) {
  digit[0] = counter % 10;
  sprintf(buff[0], "%d", digit[0]);

  digit[1] = (counter / 10) % 10;
  sprintf(buff[1], "%d", digit[1]);

  digit[2] = (counter / 100) % 10;
  sprintf(buff[2], "%d", digit[2]);

  digit[3] = (counter / 1000) % 10;
  sprintf(buff[3], "%d", digit[3]);

  int i = 0;
  while (i < 8)
  {
    if (millis() - timer2 > delay1) {
      timer2 = millis();
      for (int j = 0; j < 4; j++) {
        if (digit[j] != digit_old[j])
        {
          mx.transform(j, j, MD_MAX72XX::TSU);

        }
      }
      i++;
    }
  }

  for (int j = 0; j < 4; j++) {
    if (digit[j] != digit_old[j])
    {
      printText(j, j, buff[j]);
      digit_old[j] = digit[j];
    }
  }
}

void loop()
{
  readSerial();
  if (newMessageAvailable) {
    displayValue(atoi(newMessage));
    newMessageAvailable = false;
  }





}