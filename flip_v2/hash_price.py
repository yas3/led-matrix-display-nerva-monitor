import serial
import time
import os
import re
import requests
import json
import threading
import sys

ser=serial.Serial('/dev/ttyACM0')
print("Connecting to")
print(ser.name)
time.sleep(3) 
print("Initializing...")
#ser.write("{fn:disp,value:0,error:0,alarm:on}")
#ser.write('\n')
print("Done.")
time.sleep(1) 
method="hash"

old_hash_value=0
old_price_value=0
class KeyListen (threading.Thread):
	
	
	def __init__(self):
		super(KeyListen, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread4")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
		
	def run(self):
		global old_hash_value
		global old_price_value
		global method
		alarm="off"
		while(1):
			
			nb = raw_input('')
			#print(nb)
			result=re.search('bright ([0-9]+)',nb)
			if result:
				print("PC=>Brightness:"+str(result.group(1)))
				ser.write("{fn:set,name:bright,value:%s}"%(str(result.group(1))))
				ser.write('\n')
				
			result=re.search('speed ([0-9]+)',nb)
			if result:
				print("PC=>Speed:"+str(result.group(1)))
				ser.write("{fn:set,name:speed,value:%s}"%(str(result.group(1))))
				ser.write('\n')
			result=re.search('price',nb)
			if result:
				method="price"
				print("method:"+method)
				ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(old_price_value,alarm))
				ser.write('\n')
				
			result=re.search('hash',nb)
			if result:
				method="hash"
				print("method:"+method)
				ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(old_hash_value,alarm))
				ser.write('\n')
			time.sleep(1) 

class ReadSerial (threading.Thread):
	def __init__(self):
		super(ReadSerial, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread3")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
		
	def run(self):
		global old_hash_value
		global old_price_value
		global method
		
		while(1):
			line=ser.readline()
			print("Arduino=>"+str(line))
			result=re.search('Request',line)
			result1=re.search('Wrong format',line)
			if result or result1:
					print("Resending")
					alarm="off"
					
					threadLock.acquire()
					if(method=="hash"):
						ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(old_hash_value,alarm))
					elif(method=="price"):
						ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(old_price_value,alarm))
					ser.write('\n')
					threadLock.release()
			
			#time.sleep(1) 
			
class Online (threading.Thread):
	def __init__(self):
		super(Online, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread2")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
		
	def run(self):
		while(1):
			threadLock.acquire()
			ser.write("{fn:online}")
			ser.write('\n')
			threadLock.release()
			time.sleep(1) 
		
		
		
	
class DataSend (threading.Thread):
	
	def __init__(self):
		super(DataSend, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread1")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
        
	def run(self):
		global old_hash_value
		global old_price_value
		global method
		
		while(1):
			if(method=="price"):
				#print("price")
				try:
					r = requests.get('https://tradeogre.com/api/v1/ticker/BTC-XNV')
				except requests.exceptions.RequestException as e:  # This is the correct syntax
					print e
					time.sleep(10) 
					continue
				#r='{"price":0.00000930}'
				#data=json.dumps(r)
				#data=json.loads(r)

				if r:
					data = r.json()
					
					#print (data)
					value=float(data['price'])*100000000
					
					if(value-old_price_value>100 or value-old_price_value<-100):
						
						alarm="on"
					else:
						alarm="off"
					old_price_value=value
					
					value=("{0:.0f}".format(value))
					
					#print(value)
					threadLock.acquire()
					ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(value,alarm))
					ser.write('\n')
					threadLock.release()
				else:
					threadLock.acquire()
					ser.write("{fn:err,value:0,error:1}")
					ser.write('\n')
					threadLock.release()
				timer=0;
				while (timer<60):
					time.sleep(1)
					timer=timer+1
					if(method!="price"):break
					
			elif(method=="hash"):
				#print("hash")
				HR=0
				msg= os.popen("./nervad status").read()
				result_HR=re.search('mining at ([0-9.]+) k?H/s',msg)

				
				if result_HR:
					result_HR_k=re.search('k',result_HR.group(0))
					if result_HR_k:
						HR=float(result_HR.group(1))*1000
						
						
					else:
						HR=float(result_HR.group(1))
						
					HR=("{0:.0f}".format(HR))
					HR=int(HR)
					#print(HR)
					
					
					
					#print(result_HR.group())
					
					if(HR - old_hash_value > 100 or HR - old_hash_value < -100):
					
						alarm="on"
					else:
						alarm="off"
					old_hash_value=HR	
					threadLock.acquire()
					ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(str(HR),alarm))
					ser.write('\n')
					threadLock.release()
						
				else:
					threadLock.acquire()
					ser.write("{fn:err,value:0,error:1}")
					ser.write('\n')
					threadLock.release()
				
				timer=0;
				while (timer<20):
					time.sleep(1)
					timer=timer+1
					if(method!="hash"):break
			else:
				time.sleep(10) 
			
			
try:			
	threadLock = threading.Lock()
	thread1=DataSend()
	thread1.daemon = True
	thread1.start()
	
	thread2=Online()
	thread2.daemon = True
	thread2.start()
	
	thread3=ReadSerial()
	thread3.daemon = True
	thread3.start()
	
	thread4=KeyListen()
	thread4.daemon = True
	thread4.start()
	
	while(True):
		time.sleep(10) 

except KeyboardInterrupt:
	print "Ctrl-c pressed ..."
	thread1.stop()
	thread2.stop()
	thread3.stop()
	thread4.stop()
	sys.exit(1)
