If data not coming from pc for 5 seconds display 0 for offline indication.

Data sent from pc to arduino in json format for more functionality

# Usage

Change brightness of LED MATRIX display (values 0 to 10)

bright 4

Change up scroll speed of LED MATRIX display (values 1 to 500) higher number slower scroll speed

speed 100

## For hash_price.py 

To change to hash mode

Type hash

To change to price mode

Type price


# Features:

If arduino get reset , request for data and pc resend data

If pc send wrong format or garbage , resending data

Added a speaker for alarm if value greater than or less than 100 sat change

## Flip

To display Hash rate

![](2.gif)

Library for LED MATRIX display

https://github.com/MajicDesigns/MD_MAX72XX

Circuit Diagram

![](CIR1.JPG)

## Hardware List

Dot led matrix MCU control LED Display module MAX7219 for Arduino

https://www.ebay.com/itm/Dot-led-matrix-MCU-control-LED-Display-module-MAX7219-for-Arduino-Raspberry-Pi-M/302522228880?hash=item466fbae090:g:e9wAAOSwUwFaBAno:rk:1:pf:1&frcectupt=true

UNO R3 ATMEGA328P-16AU CH340G Micro USB With Cable Board

https://www.ebay.com/itm/UNO-R3-ATMEGA328P-16AU-CH340G-Micro-USB-With-Cable-Board-For-Arduino-NEW/263884202082?hash=item3d70b96062:g:XTEAAOSwGCNbdkzE:rk:2:pf:1&frcectupt=true

