import serial
import time
import os
import re
import requests
import json
import threading
import sys

ser=serial.Serial('/dev/ttyACM0')
print("Connecting to")
print(ser.name)
time.sleep(3) 
print("Initializing...")
ser.write("{fn:disp,value:0,error:0,alarm:on}")
ser.write('\n')
print("Done.")

old_value=0

class ReadSerial (threading.Thread):
	global old_value
	def __init__(self):
		super(ReadSerial, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread3")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
		
	def run(self):
		while(1):
			line=ser.readline()
			print(line)
			result=re.search('Request',line)
			result1=re.search('Wrong format',line)
			if result or result1:
				print("Resending")
				alarm="off"
				threadLock.acquire()
				ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(old_value,alarm))
				ser.write('\n')
				threadLock.release()
			
			#time.sleep(1) 
			
class Online (threading.Thread):
	def __init__(self):
		super(Online, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread2")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
		
	def run(self):
		while(1):
			threadLock.acquire()
			ser.write("{fn:online}")
			ser.write('\n')
			threadLock.release()
			time.sleep(1) 
		
		
		
	
class DataSend (threading.Thread):
	
	def __init__(self):
		super(DataSend, self).__init__()
		self._stop_event = threading.Event()

	def stop(self):
		print("stopping thread1")
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()
        
	def run(self):
		global old_value
		while(1):
			try:
				r = requests.get('https://tradeogre.com/api/v1/ticker/BTC-XNV')
			except requests.exceptions.RequestException as e:  # This is the correct syntax
				print e
				time.sleep(10) 
				continue
			#r='{"price":0.00000930}'
			#data=json.dumps(r)
			#data=json.loads(r)

			if r:
				data = r.json()
				
				#print (data)
				value=float(data['price'])*100000000
				
				if(value-old_value>100 or value-old_value<-100):
					
					alarm="on"
				else:
					alarm="off"
				old_value=value
				
				value=("{0:.0f}".format(value))
				
				#print(value)
				threadLock.acquire()
				ser.write("{fn:disp,value:%s,error:0,alarm:%s}"%(value,alarm))
				ser.write('\n')
				threadLock.release()
			else:
				threadLock.acquire()
				ser.write("{fn:err,value:0,error:1}")
				ser.write('\n')
				threadLock.release()
			
			
			time.sleep(60) 
try:			
	threadLock = threading.Lock()
	thread1=DataSend()
	thread1.daemon = True
	thread1.start()
	
	thread2=Online()
	thread2.daemon = True
	thread2.start()
	
	thread3=ReadSerial()
	thread3.daemon = True
	thread3.start()
	while(True):
		time.sleep(10) 

except KeyboardInterrupt:
	print "Ctrl-c pressed ..."
	thread1.stop()
	thread2.stop()
	thread3.stop()
	sys.exit(1)
